import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.*;

/**
  *
  * Beschreibung
  *
  * @version 1.0 vom 22.11.2017
  * @author 
  */

public class RGB extends Frame {
  // Anfang Attribute
  private JSlider jSlider1 = new JSlider(JSlider.HORIZONTAL,
                                      0, 255, 128);
  private JSlider jSlider2 = new JSlider(JSlider.HORIZONTAL,
                                      0, 255, 128);
  private JSlider jSlider3 = new JSlider(JSlider.HORIZONTAL,
                                      0, 255, 128);
  private JLabel jLabel1 = new JLabel();
  private JLabel jLabel2 = new JLabel();
  private JLabel jLabel3 = new JLabel();
  private JNumberField jNumberField1 = new JNumberField();
  private JNumberField jNumberField2 = new JNumberField();
  private JNumberField jNumberField3 = new JNumberField();
  private Button button1 = new Button();
  // Ende Attribute
  
  public RGB(String title) { 
    // Frame-Initialisierung
    super(title);
    addWindowListener(new WindowAdapter() {
      public void windowClosing(WindowEvent evt) { dispose(); }
    });
    int frameWidth = 347; 
    int frameHeight = 300;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    setResizable(false);
    Panel cp = new Panel(null);
    add(cp);
    // Anfang Komponenten
    
    jSlider1.setBounds(50, 150, 200, 10);
    jSlider1.addChangeListener(new ChangeListener() { 
      public void stateChanged(ChangeEvent evt) { 
        jSlider1_StateChanged(evt);
      }
    });
    cp.add(jSlider1);
    
    jSlider2.setBounds(50, 175, 200, 10);
    jSlider2.addChangeListener(new ChangeListener() { 
      public void stateChanged(ChangeEvent evt) { 
        jSlider2_StateChanged(evt);
      }
    });
    cp.add(jSlider2);
    
    jSlider3.setBounds(50, 200, 200, 10);
    jSlider3.addChangeListener(new ChangeListener() { 
      public void stateChanged(ChangeEvent evt) { 
        jSlider3_StateChanged(evt);
      }
    });
    cp.add(jSlider3);
    jLabel1.setBounds(10, 140, 38, 20);
    jLabel1.setText("R");
    cp.add(jLabel1);
    jLabel2.setBounds(10, 165, 30, 20);
    jLabel2.setText("G");
    cp.add(jLabel2);
    jLabel3.setBounds(10, 190, 38, 20);
    jLabel3.setText("B");
    cp.add(jLabel3);
    jNumberField1.setBounds(260, 150, 75, 20);
    jNumberField1.setText("128");
    jNumberField1.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        jNumberField1_ActionPerformed(evt);
      }
    });
    cp.add(jNumberField1);
    jNumberField2.setBounds(260, 175, 75, 20);
    jNumberField2.setText("128");
    jNumberField2.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        jNumberField2_ActionPerformed(evt);
      }
    });
    cp.add(jNumberField2);
    jNumberField3.setBounds(260, 200, 75, 20);
    jNumberField3.setText("128");
    jNumberField3.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        jNumberField3_ActionPerformed(evt);
      }
    });
    cp.add(jNumberField3);
    button1.setBounds(64, 24, 201, 97);
    button1.setLabel("");
    button1.addActionListener(new ActionListener() { 
      public void actionPerformed(ActionEvent evt) { 
        button1_ActionPerformed(evt);
      }
    });
    button1.setEnabled(false);
    cp.add(button1);
    addWindowListener(new WindowAdapter() { 
      public void windowOpened(WindowEvent evt) { 
        rGB_WindowOpened(evt);
      }
    });
    // Ende Komponenten
    
    setVisible(true);
  } // end of public RGB
  
  // Anfang Methoden
  
  public static void main(String[] args) {
    new RGB("RGB");
  } // end of main
  
  public void jNumberField1_ActionPerformed(ActionEvent evt) {
    if(Integer.parseInt(jNumberField1.getText()) > 255){
      jNumberField1.setText("255");
    }
    
    jSlider1.setValue(Integer.parseInt(jNumberField1.getText()));
  } // end of jNumberField1_ActionPerformed
  
  public void jNumberField2_ActionPerformed(ActionEvent evt) {
    if(Integer.parseInt(jNumberField2.getText()) > 255){
      jNumberField2.setText("255");
    }
    
    jSlider2.setValue(Integer.parseInt(jNumberField2.getText()));
  } // end of jNumberField2_ActionPerformed
  
  public void jNumberField3_ActionPerformed(ActionEvent evt) {
    if(Integer.parseInt(jNumberField3.getText()) > 255){
      jNumberField3.setText("255");
    }
    
    jSlider3.setValue(Integer.parseInt(jNumberField3.getText()));
  } // end of jNumberField3_ActionPerformed
  
  public void jSlider1_StateChanged(ChangeEvent evt) {
    jNumberField1.setText(""+jSlider1.getValue());
    button1.setBackground(new Color(jSlider1.getValue(),jSlider2.getValue(),jSlider3.getValue()));
  } // end of jSlider1_StateChanged
  
  public void jSlider2_StateChanged(ChangeEvent evt) {
    jNumberField2.setText(""+jSlider2.getValue());
    button1.setBackground(new Color(jSlider1.getValue(),jSlider2.getValue(),jSlider3.getValue()));
  } // end of jSlider2_StateChanged
  
  public void jSlider3_StateChanged(ChangeEvent evt) {
    jNumberField3.setText(""+jSlider3.getValue());
    button1.setBackground(new Color(jSlider1.getValue(),jSlider2.getValue(),jSlider3.getValue()));
  } // end of jSlider3_StateChanged
  
  public void button1_ActionPerformed(ActionEvent evt) {
    // TODO hier Quelltext einf�gen
  } // end of button1_ActionPerformed
  
  public void rGB_WindowOpened(WindowEvent evt) {
    button1.setBackground(new Color(jSlider1.getValue(),jSlider2.getValue(),jSlider3.getValue()));
  } // end of rGB_WindowOpened
  
  // Ende Methoden
} // end of class RGB
